import Vue from 'vue'
import App from './Sidebar.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})
