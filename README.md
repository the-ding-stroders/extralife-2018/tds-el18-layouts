# The Ding Stroders Extra Life 2018 Streamaganza NodeCG Bundle
tds-extralife-2018 is a [NodeCG](https://github.com/nodecg/nodecg) bundle.
It works with NodeCG versions which satisfy this [semver](https://docs.npmjs.com/getting-started/semantic-versioning) range: `^1.1.1`
You will need to have an appropriate version of NodeCG installed to use it.

---

This bundle controlled the on-stream graphics, as well as some functionality of OBS. It was created ***extremely*** quickly, so there's a lot of horrible code inside.

That being said, feel free to look around. If you have any questions, comments, or concerns, contact [synth3tk](https://twitter.com/synth3tk) on Twitter.

## TODO:
* The main plan, after catching our breath from the Extra Life streams, is to continue improving on this bundle to make it reusable for more frequent community streams.
* Write up a blog post (and maybe make videos) explaining some of the bundle.
